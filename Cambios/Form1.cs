﻿namespace Cambios
{
    using Servicos;
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Windows.Forms;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {
        #region Atributos
        private List<Rate> Rates;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;

        #endregion


        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            Rates = new List<Rate>();
            LoadRates();
        }

        private async void LoadRates()
        {
            bool load;

            LabelResultado.Text = "A actualizar taxas...";
            var connection = networkService.CheckConnection();

            if (!connection.IsSucess)
            {
                LoadLoacalRates();
                load = false;



            }
            else
            {
                await LoadApiRates();
                load = true;
            }

            if (Rates.Count == 0)
            {
                LabelResultado.Text = "Não há ligação à Internet" + Environment.NewLine +
                    "e não foram préviamente carregadas as taxas" + Environment.NewLine +
                    "Tente mais tarde!";

                LabelStatus.Text = "Primeira inicialização deverá ter ligação à internet";

                return;

            }


            ComboBoxMoedaOrigem.DataSource = Rates;
            ComboBoxMoedaOrigem.DisplayMember = "Name";

            ComboBoxMoedaDestino.BindingContext = new BindingContext();

            ComboBoxMoedaDestino.DataSource = Rates;
            ComboBoxMoedaDestino.DisplayMember = "Name";


           
            LabelResultado.Text = "Taxas atualizadas!";
          
            if (load)
            {
                LabelStatus.Text = String.Format("Taxas carregadas com sucesso da Internet em {0:F}", DateTime.Now);
            }
            else
            {
                LabelStatus.Text = String.Format("Taxas carregadas da base de dados ");
            }
            ProgressBar1.Value = 100;
            ButtonConverter.Enabled = true;
            ButtonTrocar.Enabled = true;

        }

        private void LoadLoacalRates()
        {
            Rates=dataService.GetData();
        }

        private async Task LoadApiRates()
        {
            ProgressBar1.Value = 0;

            var response = await apiService.GetRates("http://apiexchangerates.azurewebsites.net", "api/Rates");

            Rates = (List<Rate>)response.Result;

            dataService.DeleteData();
            dataService.SaveData(Rates);
        }

        private void ButtonConverter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if (string.IsNullOrEmpty(TextBoxValor.Text))
            {
                dialogService.DisplayMessage("Erro", "Introduza um valor a converter!");
                TextBoxValor.Focus();
                return;
            }
            decimal valor;
            if (!decimal.TryParse(TextBoxValor.Text, out valor))
            {
                dialogService.DisplayMessage("Erro de Conversão", "Insira um valor numérico!");
                TextBoxValor.Focus();
                return;
            }

            if (ComboBoxMoedaOrigem.SelectedItem==null)
            {
                dialogService.DisplayMessage("Erro", "Selecione a moeda de origem");
                return;
            }
            if (ComboBoxMoedaDestino.SelectedItem == null)
            {
                dialogService.DisplayMessage("Erro", "Selecione a moeda de destino");
                return;
            }

            var taxaOrigem = (Rate)ComboBoxMoedaOrigem.SelectedItem;

            var taxaDestino = (Rate)ComboBoxMoedaDestino.SelectedItem;
            var valorConvertido = valor / (decimal)taxaOrigem.TaxRate * (decimal)taxaDestino.TaxRate;

            LabelResultado.Text = string.Format("{0:N2} {1} = {2:N2} {3}",  valor, taxaOrigem.Code, valorConvertido,taxaDestino.Code);
        }

        private void ButtonTrocar_Click(object sender, EventArgs e)
        {
          Trocar();
        }

        private void Trocar()
        {
            var aux = ComboBoxMoedaOrigem.SelectedItem;
            ComboBoxMoedaOrigem.SelectedItem = ComboBoxMoedaDestino.SelectedItem;
            ComboBoxMoedaDestino.SelectedItem = aux;

            Converter();
        }
    }
}
